import {
	ApprovalForAll,
	Transfer,
} from "../generated/BoredApeYachtClub/BoredApeYachtClub";
import { BigInt, log, store } from "@graphprotocol/graph-ts";
import { Owner, Operator, Approval, Global } from "../generated/schema";

// Handles the ApprovalForAll event from the BoredApeYachtClub contract
export function handleApprovalForAll(event: ApprovalForAll): void {
	log.info("Handling ApprovalForAll event for owner {} and operator {}", [
		event.params.owner.toHex(),
		event.params.operator.toHex(),
	]);

	let ownerId = event.params.owner.toHex();
	let operatorId = event.params.operator.toHex();
	let approvalId = ownerId + "-" + operatorId;

	let owner = Owner.load(ownerId);
	let isNewOwner = false;
	if (owner == null) {
		owner = new Owner(ownerId);
		owner.balance = 0;
		owner.approvalCount = 0;
		owner.exposureScore = BigInt.fromI32(0);
		owner.save();
		isNewOwner = true;
		log.info("New Owner entity created: {}", [ownerId]);
	}

	let operator = Operator.load(operatorId);
	let isNewOperator = false;
	if (operator == null) {
		operator = new Operator(operatorId);
		operator.approvalCount = 0;
		operator.save();
		isNewOperator = true;
		log.info("New Operator entity created: {}", [operatorId]);
	}

	let stats = getOrCreateGlobal();
	stats.totalApprovalForAllEvents++;
	if (isNewOwner) {
		stats.totalUniqueWallets++;
	}
	if (isNewOperator) {
		stats.totalUniqueOperators++;
	}
	stats.save();
	log.info(
		"Global updated: Total ApprovalForAll Events: {}, Total Unique Wallets: {}, Total Unique Operators: {}",
		[
			stats.totalApprovalForAllEvents.toString(),
			stats.totalUniqueWallets.toString(),
			stats.totalUniqueOperators.toString(),
		]
	);

	if (event.params.approved) {
		operator.approvalCount++;
		operator.save();

		let approval = new Approval(approvalId);
		approval.owner = ownerId;
		approval.operator = operatorId;
		approval.save();
		log.info("Approval entity updated: {}", [approvalId]);
	} else {
		if (operator) {
			operator.approvalCount =
				operator.approvalCount > 0 ? operator.approvalCount - 1 : 0;
			operator.save();
		}

		let approval = Approval.load(approvalId);
		if (approval) {
			store.remove("Approval", approvalId);
			log.info("Approval entity removed: {}", [approvalId]);
		}
	}

	if (owner != null) {
		if (event.params.approved) {
			owner.approvalCount++;
		} else {
			owner.approvalCount =
				owner.approvalCount > 0 ? owner.approvalCount - 1 : 0;
		}

		let balanceBigInt = BigInt.fromI32(owner.balance);
		let approvalCountBigInt = BigInt.fromI32(owner.approvalCount);
		owner.exposureScore = balanceBigInt.times(approvalCountBigInt);
		owner.save();
		log.info("Owner exposureScore updated for {}: {}", [
			ownerId,
			owner.exposureScore.toString(),
		]);
	}
}

export function handleTransfer(event: Transfer): void {
	log.info("Handling Transfer event from {} to {}", [
		event.params.from.toHex(),
		event.params.to.toHex(),
	]);

	let fromId = event.params.from.toHex();
	let toId = event.params.to.toHex();

	// Handling the sender of the transfer
	let fromOwner = Owner.load(fromId);
	if (fromOwner != null) {
		fromOwner.balance = fromOwner.balance > 0 ? fromOwner.balance - 1 : 0;
		let balanceBigInt = BigInt.fromI32(fromOwner.balance);
		let approvalCountBigInt = BigInt.fromI32(fromOwner.approvalCount);
		fromOwner.exposureScore = balanceBigInt.times(approvalCountBigInt);
		fromOwner.save();
		log.info(
			"From Owner {} balance decremented to: {}, exposureScore updated to: {}",
			[
				fromId,
				fromOwner.balance.toString(),
				fromOwner.exposureScore.toString(),
			]
		);
	}

	// Handling the receiver of the transfer
	let isNewOwner = false;
	let toOwner = Owner.load(toId);
	if (toOwner == null) {
		isNewOwner = true;
		toOwner = new Owner(toId);
		toOwner.balance = 1; // Initialize balance for new owner
		toOwner.approvalCount = 0; // Initialize approvalCount for new owner
		toOwner.exposureScore = BigInt.fromI32(1).times(BigInt.fromI32(0)); // Initialize exposureScore
		log.info("New To Owner entity created: {} with initial balance: 1", [
			toId,
		]);
	} else {
		toOwner.balance++;
		let balanceBigInt = BigInt.fromI32(toOwner.balance);
		let approvalCountBigInt = BigInt.fromI32(toOwner.approvalCount);
		toOwner.exposureScore = balanceBigInt.times(approvalCountBigInt);
		log.info(
			"To Owner {} balance incremented to: {}, exposureScore updated to: {}",
			[toId, toOwner.balance.toString(), toOwner.exposureScore.toString()]
		);
	}
	toOwner.save();

	// Update Global Stats if new owner detected
	if (isNewOwner) {
		let stats = getOrCreateGlobal();
		stats.totalUniqueWallets++;
		stats.save();
		log.info("Global stats updated: Total Unique Wallets: {}", [
			stats.totalUniqueWallets.toString(),
		]);
	}
}

// Utility function to get or create the Global entity
function getOrCreateGlobal(): Global {
	let stats = Global.load("GLOBAL_STATS");
	if (stats == null) {
		stats = new Global("GLOBAL_STATS");
		stats.totalApprovalForAllEvents = 0;
		stats.totalUniqueWallets = 0;
		stats.totalUniqueOperators = 0;
		stats.save();
	}
	return stats as Global;
}
